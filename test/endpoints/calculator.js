require('dotenv').config();
const { server } = require('../ConfigMocha');
const supertest = require('supertest');

describe('calculator-Validatefields', () => {
  it('Should return (200)', (done) => {
    supertest(server)
      .get('/calculator?from=96060160&height=280&lenght=20&to=05458070&weight=3.2&width=20&ar=false&mp=true')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.an.instanceOf(Object);
        done();
      });
  });
});

describe('calculator-Validate-incorrect-cep', () => {
  it('Should return cep error (404)', (done) => {
    supertest(server)
      .get('/calculator?from=99999999&height=280&lenght=20&to=05458070&weight=3.2&width=20&ar=false&mp=true')
      .end((err, res) => {
        res.should.have.status(404);
        res.should.be.an.instanceOf(Object);
        done();
      });
  });
});
