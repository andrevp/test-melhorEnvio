const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../src/app');

const should = chai.should();

chai.use(chaiHttp);

module.exports = {
  chai,
  should,
  server,
};

