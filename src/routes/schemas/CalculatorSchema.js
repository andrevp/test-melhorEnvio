const Joi = require('joi');
const RouteValidator = require('../../middlewares/RouteValidator');

class CalculatorSchema extends RouteValidator {
  static get get() {
    const schema = {
      query: Joi.object().keys({
        from: Joi.string().regex(/[0-9]+/).length(8)
          .required(),
        height: Joi.number().integer().required(),
        lenght: Joi.number().integer().required(),
        to: Joi.string().regex(/[0-9]+/).length(8)
          .required(),
        weight: Joi.number().required(),
        width: Joi.number().integer().required(),
        value: Joi.number(),
        ar: Joi.boolean().required(),
        mp: Joi.boolean().required(),
      }),
    };
    return this.validate(schema);
  }
}

module.exports = CalculatorSchema;
