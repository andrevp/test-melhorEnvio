const express = require('express');
const CalculatorController = require('../controllers/CalculatorController');
const CalculatorSchema = require('../routes/schemas/CalculatorSchema');

const router = express.Router({ mergeParams: true });

/* GET /calculator */
router.get('/', CalculatorSchema.get, CalculatorController.get);

module.exports = router;
