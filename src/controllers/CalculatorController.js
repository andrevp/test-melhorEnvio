const Logger = require('../helpers/Logger');
const CalculatorService = require('../services/CalculatorService');

class CalculatorController {
  static async get(req, res) {
    try {
      const conveyors = await CalculatorService.get(req.query);

      if (conveyors.success === false) {
        return res
          .status(404)
          .send({
            success: false,
            message: conveyors.message,
          });
      }

      return res
        .status(200)
        .send({
          success: true,
          data: conveyors,
        });
    } catch (err) {
      Logger.throw(res, '6001059324', err);
    }
  }
}

module.exports = CalculatorController;
