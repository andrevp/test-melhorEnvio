const { knex } = require('../config/db');
const ExpressModel = require('../models/ExpressModel');
const EconomicModel = require('../models/EconomicModel');
const _ = require('lodash');

class OrderService {
  static async getOrderWeight(data) {
    const cubicWeight = Math.ceil((data.lenght * data.width * data.height) / 6000);
    const realWeight = Math.ceil(data.weight);
    
    if (cubicWeight <= 10) {
      return realWeight;
    }
    return Math.max(cubicWeight, realWeight);
  }
  
  static async getAvailableShipments(ordersData) {
    const {
      data,
      wayCode,
      orderWeight,
    } = ordersData;
    
    const orderWeightInGr = orderWeight * 1000;
    
    const expressService = await OrderService._checkExpressConditions(ordersData, orderWeightInGr);
    const economicService = await OrderService._checkEconomicConditions(ordersData, orderWeightInGr);
    return {
      expresso:{
        ...expressService  
      },
      economico:{
        ...economicService
      }
    };
  }
  
  static async _checkExpressConditions(ordersData, weightGr) {
    const {
      wayCode,
      orderWeight,
      orderWeightInGr,
    } = ordersData;
    
    const expressValidation = await ExpressModel.validateDimensions({ ordersData, weightGr });
    if (expressValidation.success === false) {
      return expressValidation;
    }
    
    const isShipmentToWayCode = await ExpressModel.listColumns();
    
    if (typeof isShipmentToWayCode[0][wayCode] === 'undefined') {
      return {
        sucess: false,
        message: 'Não entrega nesta localização',
      };
    }
   
    const listAvailableExpress = await ExpressModel.get(weightGr);
   
    if (!listAvailableExpress.length) {
      const maxWeigthFomExpress = await ExpressModel.getMaxWeight();
      const additionalWeightPrices = await ExpressModel.getExtraValues();
      
      const restOfWeight = (weightGr - maxWeigthFomExpress.Peso_gr_max) / 1000;
      
      const priceByWeight = maxWeigthFomExpress[wayCode];
      
      const priceAdditional = additionalWeightPrices[0][wayCode] * restOfWeight;
      
      const totalPrice = parseFloat(priceAdditional) + parseFloat(priceByWeight);
      return { totalPrice };
    }
    const totalPrice = parseFloat(listAvailableExpress[0][wayCode]);
    return {totalPrice}
  }
  
  static async _checkEconomicConditions(ordersData, weightGr) {
    const {
      wayCode,
      orderWeight,
      orderWeightInGr,
    } = ordersData;
    
    const economicValidation = await EconomicModel.validateDimensions({ ordersData, weightGr });
   
    if (economicValidation.success === false) {
      return economicValidation;
    }
    
    const isShipmentToWayCode = await EconomicModel.listColumns();
    
    if (typeof isShipmentToWayCode[0][wayCode] === 'undefined') {
      return {
        sucess: false,
        message: 'Não entrega nesta localização',
      };
    }
    
    const listAvailableEconomic = await EconomicModel.get(weightGr);
    
    if (!listAvailableEconomic.length) {
      const maxWeigthFomEconomic = await EconomicModel.getMaxWeight();
      
      const additionalWeightPrices = await EconomicModel.getExtraValues();
      
      const restOfWeight = (weightGr - maxWeigthFomEconomic.Peso_gr_max) / 1000;
      
      const priceByWeight = maxWeigthFomEconomic[wayCode];
      
      const priceAdditional = additionalWeightPrices[0][wayCode] * restOfWeight;
      
      const totalPrice = parseFloat(priceAdditional) + parseFloat(priceByWeight);
      return { totalPrice };
    }
    const totalPrice = parseFloat(listAvailableEconomic[0][wayCode]);
    return {totalPrice}
  }
}

module.exports = OrderService;
