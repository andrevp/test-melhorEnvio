const LocationService = require('../services/LocationService');
const OrderService = require('../services/OrderService');

class CalculatorService {
  static async get(data) {
    const fromLocation = await LocationService.get(data.from);
    if (!fromLocation) {
      return {
        success: false,
        message: 'CEP de origem incorreto',
      };
    }

    const toLocation = await LocationService.get(data.to);
    if (!toLocation) {
      return {
        success: false,
        message: 'CEP de destino incorreto',
      };
    }

    const wayCode = await LocationService.getWayCode({ fromLocation, toLocation });

    const orderWeight = await OrderService.getOrderWeight(data);

    const orderPricing = await OrderService.getAvailableShipments({ data, wayCode, orderWeight });
    return CalculatorService._configureAdditionalServices(orderPricing, data);

    // return orderPricing;
  }

  static async _configureAdditionalServices(orderPricing, data) {
    const {
      expresso,
      economico,
    } = orderPricing;

    if (expresso.totalPrice) {
      let totalExpresso = expresso.totalPrice;
      if (data.value === 'true') {
        totalExpresso += (data.value / 100) * 1;
      }
      if (data.ar === 'true') {
        totalExpresso += (expresso.totalPrice / 100) * 5;
      }
      if (data.mp === 'true') {
        totalExpresso += 3;
      }

      expresso.totalPrice = totalExpresso.toFixed(2);
    }

    if (economico.totalPrice) {
      let totalEconomic = economico.totalPrice;
      if (data.value === 'true') {
        totalEconomic += (data.value / 100) * 1;
      }
      if (data.ar === 'true') {
        totalEconomic += (economico.totalPrice / 100) * 5;
      }
      if (data.mp === 'true') {
        totalEconomic += 3;
      }
      economico.totalPrice = totalEconomic.toFixed(2);
     
    }
    return {
      expresso,
      economico,
    };
  }
}

module.exports = CalculatorService;
