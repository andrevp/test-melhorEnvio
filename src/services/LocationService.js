const fetch = require('node-fetch');
const _ = require('lodash');
const MatrixModel = require('../models/MatrixModel');

class LocationService {
  static async get(data) {
    const addressData = await LocationService._findLocation(data);
    if (addressData.error) {
      return null;
    }
    addressData.isCapital = await LocationService._isCapital(addressData.ibge);

    return addressData;
  }

  static async _findLocation(cep) {
    try {
      const response = await fetch(`https://location.melhorenvio.com.br/${cep}`);
      const address = await response.json();
      return address;
    } catch (error) {
      console.log(error);
    }
  }

  static async _isCapital(ibgeCode) {
    const ibgeCapitals = [2800308, 1501402, 3106200, 1400100, 5300108,
      5002704, 5103403, 4106902, 4205407, 2304400, 5208707, 2507507,
      1600303, 2704302, 1302603, 2408102, 1721000, 4314902, 1100205,
      2611606, 1200401, 3304557, 2927408, 2111300, 3550308, 2211001,
      3205309];

    return (ibgeCapitals.indexOf(parseInt(ibgeCode)) > 0);
  }

  static async getWayCode(data) {
    const {
      toLocation,
      fromLocation,
    } = data;

    // mesma cidade
    if (fromLocation.ibge === toLocation.ibge) {
      const matrixKey = _.findKey(MatrixModel, { ORIG: fromLocation.uf });
      const singleArray = MatrixModel[matrixKey];
      return `L${singleArray[toLocation.uf][1]}`;
    }

    // mesmo estado
    if (fromLocation.uf === toLocation.uf) {
      const matrixKey = _.findKey(MatrixModel, { ORIG: fromLocation.uf });
      const singleArray = MatrixModel[matrixKey];
      return singleArray[toLocation.uf];
    }

    // capitais
    if (fromLocation.isCapital && toLocation.isCapital) {
      const matrixKey = _.findKey(MatrixModel, { ORIG: fromLocation.uf });
      const singleArray = MatrixModel[matrixKey];
      return `N${singleArray[toLocation.uf]}`;
    }

    // interestatuduais
    if (fromLocation.uf !== toLocation.uf) {
      const matrixKey = _.findKey(MatrixModel, { ORIG: fromLocation.uf });
      const singleArray = MatrixModel[matrixKey];
      return `I${singleArray[toLocation.uf]}`;
    }
  }
}
module.exports = LocationService;

