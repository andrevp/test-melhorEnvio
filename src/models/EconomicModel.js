const { knex } = require('../config/db');

class EconomicModel {
  static get(weight) {
    return knex
      .where('Peso_gr', '<=', weight)
      .andWhere('Peso_gr_max', '>=', weight)
      .from('economic');
  }

  static getMaxWeight() {
    return knex
      .first()
      .orderBy('Peso_gr_max', 'desc')
      .from('economic');
  }

  static getExtraValues() {
    return knex
      .where('Peso_gr', '=', 0)
      .andWhere('Peso_gr_max', '=', 0)
      .from('economic');
  }

  static listColumns() {
    return knex
      .limit(1)
      .from('economic');
  }

  static validateDimensions(order) {
    const {
      ordersData,
      weightGr,
    } = order;
    const {
      data,
      wayCode,
      orderWeight,
    } = ordersData;

    if (data.height < 2 || data.height > 105) {
      return {
        success: false,
        message: 'Altura não permitida, disponivel entre 2 à 105 cm',
      };
    }
    if (data.width < 11 || data.width > 105) {
      return {
        success: false,
        message: 'Largura não permitida, disponivel entre 11 à 105 cm',
      };
    }
    if (data.lenght < 16 || data.lenght > 105) {
      return {
        success: false,
        message: 'Comprimento não permitido, disponivel entre 16 à 105 cm',
      };
    }
    if (data.orderWeight < 1 || data.orderWeight > 30) {
      return {
        success: false,
        message: 'Peso não permitido, disponivel entre 1 à 30 kg',
      };
    }
    if (data.value) {
      if (data.value < 17 || data.value > 3000) {
        return {
          success: false,
          message: 'Valor seguro não permitido, disponivel entre 17 à 3000 R$',
        };
      }
    }
    return {
      success: true,
    };
  }
}

module.exports = EconomicModel;
