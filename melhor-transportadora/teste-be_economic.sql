-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: teste-be
-- ------------------------------------------------------
-- Server version	5.7.22-0ubuntu18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `economic`
--

DROP TABLE IF EXISTS `economic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `economic` (
  `Peso_gr` int(11) DEFAULT NULL,
  `Peso_gr_max` int(11) DEFAULT NULL,
  `E1` decimal(4,2) DEFAULT NULL,
  `E2` decimal(4,2) DEFAULT NULL,
  `E3` decimal(4,2) DEFAULT NULL,
  `E4` decimal(4,2) DEFAULT NULL,
  `N1` decimal(4,2) DEFAULT NULL,
  `N2` decimal(4,2) DEFAULT NULL,
  `N3` decimal(4,2) DEFAULT NULL,
  `N4` decimal(4,2) DEFAULT NULL,
  `N5` decimal(4,2) DEFAULT NULL,
  `N6` decimal(4,2) DEFAULT NULL,
  `I1` decimal(4,2) DEFAULT NULL,
  `I2` decimal(4,2) DEFAULT NULL,
  `I3` decimal(4,2) DEFAULT NULL,
  `I4` decimal(4,2) DEFAULT NULL,
  `I5` decimal(4,2) DEFAULT NULL,
  `I6` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `economic`
--

LOCK TABLES `economic` WRITE;
/*!40000 ALTER TABLE `economic` DISABLE KEYS */;
INSERT INTO `economic` VALUES (0,500,11.85,12.35,12.48,12.60,14.10,15.79,17.63,21.15,25.38,31.02,15.10,16.79,25.63,34.15,42.38,54.02),(501,1000,12.69,13.23,13.37,13.50,15.11,16.92,18.89,22.67,27.20,33.24,16.11,17.92,26.89,35.67,44.20,56.24),(1001,2000,13.37,13.94,14.08,14.22,16.60,18.59,20.75,24.90,29.88,36.52,17.60,19.59,28.75,37.90,46.88,59.52),(2001,3000,15.98,16.66,16.83,17.00,19.84,22.22,24.80,29.76,35.71,43.65,20.84,23.22,32.80,42.76,52.71,66.65),(3001,4000,17.06,17.79,17.97,18.15,21.19,23.73,26.49,31.79,38.14,46.62,25.19,27.73,37.49,47.79,58.14,72.62),(4001,5000,18.24,19.01,19.21,19.40,22.67,25.39,28.34,34.01,40.81,49.87,26.67,29.39,39.34,50.01,60.81,75.87),(5001,6000,19.23,20.05,20.26,20.46,25.10,28.87,32.94,40.79,50.20,62.75,31.10,34.87,45.94,58.79,72.20,90.75),(6001,7000,20.32,21.19,21.40,21.62,27.71,31.87,36.37,45.03,55.42,69.28,33.71,37.87,49.37,63.03,77.42,97.28),(7001,8000,21.36,22.27,22.49,22.72,30.19,34.72,39.62,49.06,60.38,75.48,38.19,42.72,54.62,69.06,84.38,105.48),(8001,9000,21.98,22.91,23.15,23.38,31.68,36.43,41.58,51.48,63.36,79.20,39.68,44.43,56.58,71.48,87.36,109.20),(9001,10000,22.42,23.37,23.61,23.85,32.74,37.65,42.97,53.20,65.48,81.85,40.74,45.65,57.97,73.20,89.48,111.85),(0,0,2.24,2.34,2.36,2.39,3.27,3.77,4.30,5.32,6.55,8.19,4.07,4.57,5.80,7.32,8.95,11.19);
/*!40000 ALTER TABLE `economic` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-04  8:52:46
