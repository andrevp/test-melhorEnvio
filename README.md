# TESTE MELHOR ENVIO!

## Instalação

As tabelas para importação ("economico" e "expresso") para o DB Mysql encontram-se na pasta `melhor-transportadora` 

Feito a importação, favor atualizar os pacotes do npm: `npm install`

Criar o arquivo `.env` na raiz do projeto com os parâmetros necessários (consultar `.env.sample`)

Para executar a aplicação, é só executar `npm start` (necessita instalação do pacote pm2), ou `npm run dev` (necessita instalação do pacote nodemon)
Outros comandos disponiveis no `package.json`

### Estrutura API

GET /calculator

|QUERY PARAMS                  |DESCRIÇÃO  | TIPO  |
|------------------|--------|--------------------|
| from       | cep de origem da encomenda | string(8)
| height      | largura, em cm     | integer                |
| lenght | comprimento, em cm     | integer                |
| to        | cep de destino da encomenda      | string(8) |
| weight      | peso, em Kg  | number         |
| widht      | largura, em cm  | integer         |
| ar      | Aviso de recebimento  | boolean         |
| mp      | mão própria  | boolean         |
| value      | valor segurado  | number         |


### Tecnologias Utilizadas
* NodeJS 8.11.2
* MySql 5.7
